---
layout: markdown_page
title: "People Operations"
---

## Entering New Hires into TriNet<a name="trinet-process"></a>

Employer enters the employee data in the HR Passport with the information below

1. Under the My Staff tab- select new hire/rehire and a drop down menu will appear.
1. Enter all of the necessary information:
    * Company name autopopulates
    * SS
    * Form of address for hire (Mr. Ms, etc.)
    * First name
    * Last name
    * Middle name or initial
    * Country
    * Address
    * Home phone
    * Home email
    * Gener
    * Ethnicity (you must select something - guess if employee declines to state)
    * Military status

At the bottom of the screen, select next

    * TriNet’s start date
    * Reason - drop down menu with options
    * Employment type - Full time or PT options
    * Select reg/temp bubble
    * Employee Class - drop down between regular and commission
    * Estimated annual wages (does not include anything besides base salary)
    * Benefit class
    * Future benefits class -
    * Standard Hours/week - Part time or Full time
    * Business Title - see org chart
    * Job Code - no need to enter anything here
    * FLSA status- drop down options are exempt, non-exempt, computer prof-non-exempt, computer prof- exempt
    * Supervisor - drop down menu of names
    * Compensation Basis
    * Compensation Rate
    * Departments
    * Work Location - drop down menu
    * Pay Group - only one option
    * Employee ID - not necessary
    * Work email
    * Grouping A/level - not necessary
    * Grouping B/sponsor- not necessary

Select next or save (if you select save, it will hold your information)

    * Vacation/PTO - drop down menu only provides one option- select this
    * Sick- drop down menu only provides one option- select this
    * Personal Time - leave blank
    * Floating Holidays - leave blank
    * Birthdate - mm/dd/yyyy
    * Workers compensation- select unknown and it will default to our principle class code for our industry
Window: Describe employees job duties - simple description

After submission -  you will receive a prompt for final submission, select and submit.

Note: if you save the information to finish at a later date, go to the Work Inbox and select New Hires Not Submitted to continue.

1. The employee receives a welcome email the night before their start date.
1. The employee is prompted to log on, complete tax withholding (W4 data), direct deposit information, section 1 of the I-9, and benefits election (if eligible).
1. The employer logs in to HR Passport and is prompted by way of work inbox item, to complete section 2 of the I-9.

## GitLab, Inc. Employee Location

### Add a New Location
1. Go to HR Passport homepage
1. Click Find
1. Click Find Location.
1. When search field appears, leave blank and click Search.
1. Click on Add location.
1. Complete location information. For a remote location, enter the location (ex. WA remote) in all fields except city, state and zip.
1. Click Add.

### Transfer Employee to Different Location
1. Go to HR Passport homepage.
1. Click Find.
1. Select find person by Name.
1. Type the name, click search.
1. From the choices, select the name.
1. On the left side of the screen, select Employment Data.
1. Select Employee Transfer.
1. Change location and fill in necessary information.
1. Select Update.

## Setting up new hiring contracts
New team hire contracts are found on the [Contracts](https://about.gitlab.com/handbook/contracts/) page, including instructions on how to set up new contracts.
